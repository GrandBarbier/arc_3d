﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandDrag : MonoBehaviour
{
    public Transform dragPoint;
    public float dragDistance;
    public LayerMask dragMask;
    public bool dragging;

    public Transform actual;
    public Transform start;

    public float speed;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Drag"))
        {
            //check if player is dragging the rope
            dragging = Physics.CheckSphere(dragPoint.position, dragDistance, dragMask);

            if (dragging)
            {
                //set the rope position to the player hand position
                actual.position = new Vector3(dragPoint.position.x, actual.position.y, actual.position.z);
            }
        }
        else
        {
            //return of the rope to his initial position
            actual.position = Vector3.MoveTowards(actual.position, start.position, Time.deltaTime * speed);
        }
    }
}
