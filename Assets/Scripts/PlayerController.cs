﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject rightHand;
    public GameObject leftHand;

    public float sensitivity;
    
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    
    void Update()
    {
        //set values of the right hand
        var rightX = Input.GetAxis("RHorizontal") * sensitivity * Time.deltaTime;
        var rightY = Input.GetAxis("RVertical") * sensitivity * Time.deltaTime;
        //set bool to unlock more directions
        var rightZ = Input.GetButton("RForward");

        //set values of the left hand
        var leftX = Input.GetAxis("LHorizontal") * sensitivity * Time.deltaTime;
        var leftY = Input.GetAxis("LVertical") * sensitivity * Time.deltaTime;
        var leftZ = Input.GetAxis("LForward") * sensitivity * Time.deltaTime;

        //set bool to unlock rotation
        var rightRot = Input.GetButton("RRot");
        var leftRot = Input.GetButton("LRot");

        //set rotation of right hand
        if (rightRot)
        {
            if (rightZ)
            {
                rightHand.transform.Rotate(new Vector3(rightY,0,rightX)*10);
            }
            else
            {
                rightHand.transform.Rotate(new Vector3(rightY,rightX,0)*10);
            }
        }
        //set position of right hand
        else
        {
            if (rightZ)
            {
                rightHand.transform.Translate(-rightX,0,-rightY);
            }
            else
            {
                rightHand.transform.Translate(-rightX,rightY,0);
            }
        }

        //set rotation of left hand
        if (leftRot)
        {
            leftHand.transform.Rotate(new Vector3(leftX,leftZ,leftY)*10);
        }
        //set position of left hand
        else
        {
            leftHand.transform.Translate(new Vector3(leftY,leftZ,leftX)*10);
        }
    }
}
