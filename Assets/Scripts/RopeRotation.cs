﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeRotation : MonoBehaviour
{
    public Transform start;
    public Transform end;
    public Transform actual;
    public Transform rodeTop;
    public Transform rodeBot;

    public float topStart;
    public float topActual;
    public float topAngle;
    
    
    void Start()
    {
        
    }

    
    void Update()
    {
        //set vectors values
        topStart = (rodeTop.position - start.position).magnitude;
        topActual = (rodeTop.position - actual.position).magnitude;
        
        //set angle value
        topAngle = Mathf.Acos(topStart / topActual);
        topAngle *= Mathf.Rad2Deg;
        
        //set rotation of each sides of the rope
        rodeTop.rotation = Quaternion.Euler(0, 0, topAngle + 90);
        rodeBot.rotation = Quaternion.Euler(0, 0, -topAngle + 90);

        //set limits of the position of actualPos
        if (actual.localPosition.y >= start.localPosition.y)
        {
            actual.localPosition = start.localPosition;
        }

        if (actual.localPosition.y <= end.localPosition.y)
        {
            actual.localPosition = end.localPosition;
        }
        
    }
    
}
